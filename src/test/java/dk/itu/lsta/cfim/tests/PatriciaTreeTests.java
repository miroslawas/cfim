package dk.itu.lsta.cfim.tests;

import java.nio.charset.Charset;

import junit.framework.TestCase;

import org.junit.Test;

import dk.itu.lsta.cfim.Constants;
import dk.itu.lsta.cfim.entities.ItemSupportTable;
import dk.itu.lsta.cfim.entities.PatriciaTree;
import dk.itu.lsta.cfim.entities.TransactionList;
import dk.itu.lsta.cfim.io.IOHelper;
import dk.itu.lsta.cfim.logic.PatriciaTreeManager;


public class PatriciaTreeTests extends TestCase{

	private PatriciaTreeManager treeManager;

	@Override
	protected void setUp() throws Exception {
		treeManager = new PatriciaTreeManager();
		super.setUp();
	}
	
	@Test
	public void testPatriciaTree2() {
		String treeId = "2";
		String testHash = IOHelper.readFile(Constants.baseFolder + Constants.patriciaTreeTestFolder + String.format("input%d.dat", treeId), Charset.defaultCharset());
		PatriciaTree tree = buildPatriciaTree(treeId);
		assertEquals(testHash, tree.getHashValue());
	}

	private PatriciaTree buildPatriciaTree(String patriciaTreeId){
		TransactionList transactions = IOHelper.loadTransactions(Constants.patriciaTreeTestFolder + String.format("input%d.dat", patriciaTreeId));
		ItemSupportTable itemSupports = new ItemSupportTable(transactions);
		transactions = transactions.removeInfrequentItems(itemSupports, Constants.minSupport);
		//transactions.orderTransactions(new TransactionListComparator(), itemSupports);
		PatriciaTree resultTree = treeManager.buildTree(transactions, itemSupports, false);
		resultTree.updateTreeInfo();
		return resultTree;
	}
}
