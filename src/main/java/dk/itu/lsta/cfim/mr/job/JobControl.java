package dk.itu.lsta.cfim.mr.job;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.hdfs.server.namenode.top.TopConf;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import dk.itu.lsta.cfim.entities.PatriciaTree;
import dk.itu.lsta.cfim.entities.Tuple;
import dk.itu.lsta.cfim.hbase.HBaseConnectionManager;
import dk.itu.lsta.cfim.interfaces.ITreeManager;
import dk.itu.lsta.cfim.io.SerializationManager;
import dk.itu.lsta.cfim.logic.PatriciaTreeManager;
import dk.itu.lsta.cfim.mr.map.DeserializableTextInputFormat;
import dk.itu.lsta.cfim.mr.map.PatriciaIdentityMapper;
import dk.itu.lsta.cfim.mr.map.TransactionInputFormat;
import dk.itu.lsta.cfim.mr.map.TransactionsMapper;
import dk.itu.lsta.cfim.mr.reduce.PatriciaReducer;

public class JobControl {

	private final static String outputBaseDir = "/home/cloudera/datasets/cfim";
	private final static String inputDir = outputBaseDir + "/input";
	private final static String outputDir = outputBaseDir + "/output";

	public static final Log log = LogFactory.getLog(TransactionsMapper.class);
	
	public static void main(String[] args) throws Exception {

		ITreeManager patriciaManager = new PatriciaTreeManager(); 
		int currentIteration = 0;
		int roundCount;

		List<String> informations = new ArrayList<String>();
		long start = new Date().getTime();
		
		PrintStream out = new PrintStream(new FileOutputStream("output.txt"));
		System.setOut(out);
		
		long firstJobStart = new Date().getTime();
		
		Configuration conf = createConfiguration(currentIteration);
		cleanEnvironment(conf);
		Job job = new Job(conf, "cfim");

		//Input and output format configuration
		job.setMapperClass(TransactionsMapper.class);
		job.setReducerClass(PatriciaReducer.class);

		job.setInputFormatClass(TransactionInputFormat.class);
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(Text.class);

		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);

		double roundLog = Math.log(getRoundCount(conf));
		double log2 = Math.log(MRConstants.mergeFactorValue);

		roundCount = (int) Math.ceil(roundLog / log2);

		String inputPath = "/home/cloudera/datasets/input";
		FileInputFormat.addInputPath(job, new Path(inputPath));
		String outputPathStringValue = String.format(MRConstants.outputPathFormat, outputDir, currentIteration);
		Path outputPath = new Path(outputPathStringValue );
		FileOutputFormat.setOutputPath(job, outputPath);
		MultipleOutputs.addNamedOutput(job, "key", TextOutputFormat.class, LongWritable.class, Text.class);

		job.waitForCompletion(true);
		if (!jobSuccessfull(job, outputPath)){
			Thread.sleep(50);
		}
		
		long firstJobEnd = new Date().getTime() - firstJobStart;
		informations.add(String.format("First mapper ran for %s milliseconds", firstJobEnd));

		int i;
		for (i = currentIteration; i < roundCount; i++){
			long consecutiveJobStart = new Date().getTime();
			Configuration newConf = createConfiguration(currentIteration);
			Job newJob = new Job(newConf, String.format("cfim-%d", i));

			newJob.setMapperClass(PatriciaIdentityMapper.class);
			newJob.setReducerClass(PatriciaReducer.class);

			newJob.setInputFormatClass(DeserializableTextInputFormat.class);
			newJob.setMapOutputKeyClass(LongWritable.class);
			newJob.setMapOutputValueClass(Text.class);

			newJob.setOutputKeyClass(NullWritable.class);
			newJob.setOutputValueClass(Text.class);
			MultipleOutputs.addNamedOutput(newJob, "key", TextOutputFormat.class, LongWritable.class, Text.class);

			inputPath = String.format(MRConstants.outputPathFormat, outputDir, i);
			FileInputFormat.addInputPath(newJob, new Path(inputPath));
			outputPath = new Path(String.format(MRConstants.outputPathFormat, outputDir, i + 1));
			FileOutputFormat.setOutputPath(newJob, outputPath);

			newJob.waitForCompletion(true);
			if (!jobSuccessfull(newJob, outputPath)){
				Thread.sleep(50);
			}
			
			long consecutiveJobEnd = new Date().getTime() - consecutiveJobStart;
			informations.add(String.format("Job ran for %s milliseconds", consecutiveJobEnd));
		}

		long topKStart = new Date().getTime();
		
		PatriciaTree globalTree = importPatriciaTree(job, outputPath);		
		for(Tuple<String, Integer> item : patriciaManager.minTopKValues(globalTree, 10)){
			log.info(String.format("%s - %d", item.item1, item.item2));
		}
		
		long topKEnd = new Date().getTime() - topKStart;
		informations.add(String.format("TopKMiner ran for %s milliseconds", topKEnd));

		long end = new Date().getTime() - start;
		informations.add(String.format("Complete program ran for %s milliseconds", end));
		
		for (String info : informations) {
			log.info(info);
		}
	}

	private static PatriciaTree importPatriciaTree(Job job, Path patriciaFilePath) {
		try {
			FileSystem fileSystem = patriciaFilePath.getFileSystem(job.getConfiguration());
			FileStatus[] files = fileSystem.listStatus(patriciaFilePath, new PathFilter() {

				@Override
				public boolean accept(Path path) {
					if (path.toString().contains(MRConstants.reduceResultValue)){
						return true;
					}

					return false;
				}
			});
			
			if (files.length > 1){
				return null;
			}

			byte[] contents = new byte[(int) files[0].getLen()];
			FSDataInputStream in = fileSystem.open(files[0].getPath());
			IOUtils.readFully(in,  contents, 0, (int)files[0].getLen());
			return new SerializationManager().deserializePatriciaTree(new String(contents));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	private static boolean jobSuccessfull(Job job, Path outputPath) {
		try {
			FileSystem fileSystem = outputPath.getFileSystem(job.getConfiguration());
			RemoteIterator<LocatedFileStatus> listFiles = fileSystem.listFiles(outputPath, false);
			String successFilePath = (outputPath + "/_success").toLowerCase();
			while (listFiles.hasNext()){
				LocatedFileStatus fileStatus = listFiles.next();
				String filePath =fileStatus.getPath().toString().toLowerCase(); 
				if(filePath.indexOf(successFilePath) > 0){
					return true;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	private static int getRoundCount(Configuration conf) {
		Path inputFolderPath = new Path("/home/cloudera/datasets/input");
		try {
			FileSystem fs = inputFolderPath.getFileSystem(conf);
			int fileCount = 0;
			RemoteIterator<LocatedFileStatus> files = fs.listFiles(inputFolderPath, true);
			while(files.hasNext()){
				files.next();
				fileCount++;
			}

			return fileCount;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
	}

	private static Configuration createConfiguration(int currentIteration) {
		Configuration conf = new Configuration();

		//Cleaning hbase
		(new HBaseConnectionManager()).clearTable(MRConstants.tableIdentifierValue, MRConstants.rowIdentifierValue);
		(new HBaseConnectionManager()).clearTable("inputFiles", "input");

		//Setting MR constants
		conf.setBoolean(MRConstants.debugModeSpecifier, MRConstants.debuModeValue);
		conf.setInt(MRConstants.mergeFactorSpecifier, MRConstants.mergeFactorValue);
		conf.setStrings(MRConstants.reduceResultIdentifier, MRConstants.reduceResultValue);
		conf.setStrings(MRConstants.tableIdentifier, MRConstants.tableIdentifierValue);
		conf.setStrings(MRConstants.rowPrefixIdentifier, MRConstants.rowIdentifierValue);
		conf.setStrings(MRConstants.columnIdentifier, MRConstants.columnIdentifierValue);
		conf.setInt(MRConstants.columnIdentifierValue, currentIteration);

		return conf;
	}

	private static void cleanEnvironment(Configuration conf){
		Path outputPath = new Path(outputBaseDir);
		try {
			FileSystem fs = outputPath.getFileSystem(conf);
			fs.delete(outputPath, true);
			return;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.exit(-1);
	}
}

