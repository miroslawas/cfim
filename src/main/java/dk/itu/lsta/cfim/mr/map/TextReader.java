package dk.itu.lsta.cfim.mr.map;

import java.io.IOException;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import dk.itu.lsta.cfim.mr.job.MRConstants;

class TextReader extends RecordReader<LongWritable, DeserializableTextInputFormat> {
	private Path splitPath;
	private long splitLength;
	private Text value = new Text();
	private boolean textIsRead = false;
	private LongWritable key = new LongWritable();

	@Override
	public void initialize(InputSplit split, TaskAttemptContext context)
			throws IOException, InterruptedException {
		FileSplit fileSplit = (FileSplit) split;
		splitPath = fileSplit.getPath();
		splitLength = fileSplit.getLength();
		splitPath.getFileSystem(context.getConfiguration());
	}

	@Override
	public boolean nextKeyValue() throws IOException,
	InterruptedException {
		if (textIsRead){
			return false;
		}
		
		byte[] contents = new byte[(int) splitLength];
		FileSystem fs = splitPath.getFileSystem(Job.getInstance().getConfiguration());
		FSDataInputStream in = null;
		try {
			key.set(getSplitId(splitPath));
			in = fs.open(splitPath);
			IOUtils.readFully(in, contents, 0, contents.length);
			value.set(contents, 0, contents.length);	
			textIsRead = true;
			return true;
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			IOUtils.closeStream(in);
		}

		return false;
	}

	private long getSplitId(Path splitPath) {
		String replace = splitPath.toString().replace(MRConstants.reduceResultValue, "");
		String[] pathSegments = replace.split("/");
		String[] fileSegments = pathSegments[pathSegments.length - 1].trim().split("-");
		Long key = Long.parseLong(fileSegments[1]);
		return key;
	}

	@Override
	public LongWritable getCurrentKey() throws IOException,
	InterruptedException {
		return key;
	}

	@Override
	public DeserializableTextInputFormat getCurrentValue() throws IOException,
	InterruptedException {
		return new DeserializableTextInputFormat(value);
	}

	@Override
	public float getProgress() throws IOException, InterruptedException {
		return 1.0f;
	}

	@Override
	public void close() throws IOException {
	}
}
