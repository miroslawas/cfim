package dk.itu.lsta.cfim.mr.reduce;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.mortbay.log.Log;

import dk.itu.lsta.cfim.entities.PatriciaTree;
import dk.itu.lsta.cfim.interfaces.ITreeManager;
import dk.itu.lsta.cfim.io.SerializationManager;
import dk.itu.lsta.cfim.logic.PatriciaTreeManager;
import dk.itu.lsta.cfim.mr.job.MRConstants;

public class PatriciaReducer extends Reducer<LongWritable, Text, NullWritable, Text> {

	private ITreeManager treeManager;
	private SerializationManager serializationManager;
	private MultipleOutputs<NullWritable, Text> mos;
	
	@Override 
	protected void setup(Context context) throws IOException ,InterruptedException {
		treeManager = new PatriciaTreeManager();
		serializationManager = new SerializationManager();
		mos = new MultipleOutputs<NullWritable, Text>(context);
	}
	
	@Override
	protected void reduce(LongWritable key, Iterable<Text> items, Context context)
			throws IOException, InterruptedException {

		long startTime = new Date().getTime();
		long branchCount = 0;
		
		Iterator<Text> patriciaIterator = items.iterator();
		PatriciaTree tree = new PatriciaTree();

		if (patriciaIterator.hasNext()){
			Text input = patriciaIterator.next();
			tree = serializationManager.deserializePatriciaTree(input.toString());
		}
		
		while(patriciaIterator.hasNext()){
			Text input = patriciaIterator.next();
			PatriciaTree mergeableTree = serializationManager.deserializePatriciaTree(input.toString());
			tree = treeManager.mergeTree(tree, mergeableTree, false);
			branchCount += mergeableTree.getBranchCount();
		}
		
		Text outputValue = new Text(serializationManager.serializeAsJson(tree));
		mos.write("key", NullWritable.get(), outputValue, generateOutputPath(key));
		context.write(NullWritable.get(), outputValue);
		
		Log.info(String.format("Reducing %d branches in %d milliseconds", branchCount, new Date().getTime() - startTime));
	}

	@Override
	protected void cleanup(org.apache.hadoop.mapreduce.Reducer.Context context)
			throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		super.cleanup(context);
		mos.close();
	}
	
	private String generateOutputPath(LongWritable key) throws IOException {
		String outputPath = String.format("%s-%s", MRConstants.reduceResultValue, key.toString());
		return outputPath;
	}	
}
