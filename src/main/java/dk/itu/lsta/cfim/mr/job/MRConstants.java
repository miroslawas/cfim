package dk.itu.lsta.cfim.mr.job;

public class MRConstants {
	//MapReduce constants
	public static final String mergeFactorSpecifier = "cfim.prop.mr.mergeFactor";
	public static final String debugModeSpecifier = "cfim.prop.mr.isDebug";
	public static final int mergeFactorValue = 8;
	public static final boolean debuModeValue = false;
	
	//Hbase constants
	public static final String tableIdentifier = "cfim.prop.hbase.tableIdentifier";
	public static final String rowPrefixIdentifier= "cfim.prop.mr.hbase.rowPrefix";
	public static final String columnIdentifier = "cfim.prop.mr.hbase.columnIdentifier";
	
	public static final String tableIdentifierValue = "identificators";
	public static final String rowIdentifierValue = "cfim";
	public static final String columnIdentifierValue = "id";
	public static final String outputPathFormat = "%s-%d";
	public static final String reduceResultValue = "patricia-result";
	public static final String reduceResultIdentifier = "cfim.prop.mr.redOutput";
}
