package dk.itu.lsta.cfim.mr.map;

import java.io.IOException;
import java.util.Date;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.mortbay.log.Log;

import dk.itu.lsta.cfim.entities.ItemSupportTable;
import dk.itu.lsta.cfim.entities.PatriciaTree;
import dk.itu.lsta.cfim.entities.TransactionList;
import dk.itu.lsta.cfim.interfaces.ITreeManager;
import dk.itu.lsta.cfim.io.SerializationManager;
import dk.itu.lsta.cfim.logic.PatriciaTreeManager;
import dk.itu.lsta.cfim.mr.job.MRConstants;


public class TransactionsMapper extends Mapper<LongWritable, TransactionInputFormat, LongWritable, Text>{
	
	private ITreeManager treeManager;
	private SerializationManager serializationManager;
	
	public TransactionsMapper(){
		treeManager = new PatriciaTreeManager();
		serializationManager = new SerializationManager();
	}

	@Override
	protected void map(LongWritable key, TransactionInputFormat value,
			org.apache.hadoop.mapreduce.Mapper.Context context)
			throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		long startTime = new Date().getTime();
		ItemSupportTable itemSupports = new ItemSupportTable();
		PatriciaTree result = treeManager.buildTree(new TransactionList(value.getTransactions()), itemSupports,  Job.getInstance().getConfiguration().getBoolean(MRConstants.debugModeSpecifier, false));
		long newKey =  Math.floorDiv(key.get(), Job.getInstance().getConfiguration().getInt(MRConstants.mergeFactorSpecifier, 2));
		context.write(new LongWritable(newKey), new Text(serializationManager.serializeAsJson(result)));
		Log.info(String.format("Mapping %s transactions in %s milliseconds", value.getTransactions().size(), new Date().getTime() - startTime));
	}
}
