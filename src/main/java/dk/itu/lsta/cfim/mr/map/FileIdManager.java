package dk.itu.lsta.cfim.mr.map;

import java.util.List;

import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.KeyValue;

import dk.itu.lsta.cfim.hbase.HBaseConnectionManager;
import dk.itu.lsta.cfim.hbase.utilities.Helpers;
import dk.itu.lsta.cfim.interfaces.IIdManager;
import dk.itu.lsta.cfim.mr.job.MRConstants;

public class FileIdManager implements IIdManager {

	private HBaseConnectionManager hbaseConnManager;

	public FileIdManager(){
		hbaseConnManager = new HBaseConnectionManager();
	}

	@Override
	public int incrementId() {
		int latestId = getLatestId();
		if (hbaseConnManager.insertValue("inputFiles", "input", MRConstants.columnIdentifierValue, String.format("%d", latestId + 1))){
			return latestId + 1;
		}

		return 0;
	}

	@Override
	public int getLatestId() {
		Cell lastEntry = hbaseConnManager.getLastEntry("inputFiles", "input");
		if (lastEntry == null){
			return 0;
		}
		
		int cellInt = Helpers.convertCellToType(lastEntry);
		return cellInt;
	}

	@Override
	public List<Integer> getAllIds() {
		List<KeyValue> queryTable = hbaseConnManager.queryTable("inputFiles", "input");
		if (queryTable != null){
			while (queryTable.iterator() != null){
				queryTable.iterator().next();
			}
		}

		return null;
	}
}
