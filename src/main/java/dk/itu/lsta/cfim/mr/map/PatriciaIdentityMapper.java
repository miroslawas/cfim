package dk.itu.lsta.cfim.mr.map;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import dk.itu.lsta.cfim.mr.job.MRConstants;

public class PatriciaIdentityMapper extends Mapper<LongWritable, DeserializableTextInputFormat, LongWritable, Text>{

	@Override 
	protected void map(LongWritable key, DeserializableTextInputFormat value, Context context){
		try {
			long newKey =  Math.floorDiv(key.get(), context.getConfiguration().getInt(MRConstants.mergeFactorSpecifier, 2));
			context.write(new LongWritable(newKey), value.getValue());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
