package dk.itu.lsta.cfim.mr.map;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.mapred.InputSplit;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.RecordReader;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

import dk.itu.lsta.cfim.entities.Transaction;
import dk.itu.lsta.cfim.utilities.StringUtilities;

public class TransacionsRecordReader implements RecordReader {

	private boolean valueNotPassedIndicator = false;
	private boolean valueNotPassed = true;
	private static final Log LOG = LogFactory.getLog(TransacionsRecordReader.class);
	private Path file;
	private long fileLength;
	private FileSystem fs;
	private MapInputIdManager mapIdManager;
	private FSDataInputStream in = null;
	private Text textValue;
	
	public TransacionsRecordReader(JobConf jobConf, InputSplit split) {
		// Retrieve configuration, and Max allowed
		// bytes for a single record
		jobConf.getInt("mapred.linerecordreader.maxlength", Integer.MAX_VALUE);

		try {
			mapIdManager = new MapInputIdManager();
			FileSplit fileSplit = (FileSplit) split;
			file = fileSplit.getPath();
			fileLength = fileSplit.getLength();
			fs = file.getFileSystem(jobConf);
			in = fs.open(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean next(Object key, Object value) throws IOException {
		if (valueNotPassedIndicator){
			return false;
		}
		
		byte[] contents = new byte[(int) fileLength];
		
		try {
			in = fs.open(file);
			IOUtils.readFully(in, contents, 0, (int)fileLength);
			textValue.set(contents, 0, contents.length);
		} finally {
			IOUtils.closeStream(in);
		}
		
		valueNotPassedIndicator = !valueNotPassedIndicator;
		mapIdManager.incrementId();
		
		return valueNotPassed;
	}

	@Override
	public Object createKey() {
		return mapIdManager.incrementId();
	}

	@Override
	public Object createValue() {
		TransactionInputFormat transactionSplit =  new TransactionInputFormat();
		for (String transactionRaw : textValue.toString().split(System.getProperty("line.separator"))){
			Transaction transaction = new Transaction(StringUtilities.getWordsInString(transactionRaw.toString()));
			transactionSplit.addTransaction(transaction);
		}

		return transactionSplit;	
	}

	@Override
	public long getPos() throws IOException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void close() throws IOException {
		in.close();
	}

	@Override
	public float getProgress() throws IOException {
		return 1.0f;
	}
}
