package dk.itu.lsta.cfim.mr.map;

import java.io.IOException;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.mortbay.log.Log;

import dk.itu.lsta.cfim.entities.Transaction;
import dk.itu.lsta.cfim.hbase.HBaseConnectionManager;
import dk.itu.lsta.cfim.utilities.StringUtilities;

public class TransactionsReader extends RecordReader<LongWritable, TransactionInputFormat>{

	private boolean valueNotPassedIndicator = false;
	private boolean valueNotPassed = true;
	private Text value = new Text();
	private MapInputIdManager mapIdManager;
	private Path splitPath;
	private long splitLength;
	
	public TransactionsReader() {
		mapIdManager = new MapInputIdManager();
	}

	@Override
	public void initialize(InputSplit genericSplit, TaskAttemptContext context)
			throws IOException, InterruptedException {
		FileSplit split = (FileSplit) genericSplit;
		splitPath = split.getPath();
		splitLength = split.getLength();
	}

	@Override
	public void close() throws IOException {
	}

	@Override
	public LongWritable getCurrentKey() throws IOException,
	InterruptedException {
		return new LongWritable(mapIdManager.getLatestId());
	}

	@Override
	public TransactionInputFormat getCurrentValue() throws IOException, InterruptedException {
		TransactionInputFormat transactionSplit =  new TransactionInputFormat();
		String[] transactionsRaw = value.toString().split(System.getProperty("line.separator"));
		for (int i = 0; i < transactionsRaw.length; i++){
			Transaction transaction = new Transaction(StringUtilities.getWordsInString(transactionsRaw[i].toString()));
			transactionSplit.addTransaction(transaction);
		}

		Log.info(String.format("Created %d-th transactions", transactionsRaw.length));
		return transactionSplit;		
	}

	@Override
	public float getProgress() throws IOException, InterruptedException {
		if (valueNotPassedIndicator){
			return 1.0f;
		}
		
		return 0.5f;
	}

	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		if (valueNotPassedIndicator){
			return false;
		}

		FileSystem fs = splitPath.getFileSystem(Job.getInstance().getConfiguration());
		FSDataInputStream in = null;
		try {
			in = fs.open(splitPath);
			byte[] contents = new byte[(int) splitLength];
			int contentsLength = contents.length;
			IOUtils.readFully(in, contents, 0, contentsLength);
			value.set(contents, 0, contents.length);		
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		finally {
			IOUtils.closeStream(in);
		}

		valueNotPassedIndicator = !valueNotPassedIndicator;
		mapIdManager.incrementId();
		return valueNotPassed;
	}
}
