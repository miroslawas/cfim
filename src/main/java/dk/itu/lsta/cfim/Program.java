package dk.itu.lsta.cfim;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;







import dk.itu.lsta.cfim.entities.ItemSupportTable;
import dk.itu.lsta.cfim.entities.PatriciaTree;
import dk.itu.lsta.cfim.entities.TransactionList;
import dk.itu.lsta.cfim.entities.Tuple;
import dk.itu.lsta.cfim.interfaces.IIdManager;
import dk.itu.lsta.cfim.interfaces.ITreeManager;
import dk.itu.lsta.cfim.io.IOHelper;
import dk.itu.lsta.cfim.io.SerializationManager;
import dk.itu.lsta.cfim.logic.PatriciaTreeManager;
import dk.itu.lsta.cfim.utilities.TransactionListComparator;

public class Program {

	public static IIdManager idManager;
	private static ITreeManager treeManager;
	private static ArrayList<TransactionList> transactionsGlobal;
	private static SerializationManager serializationManager = new SerializationManager();
	private static PatriciaTree currentTree = new PatriciaTree();

	public static void main(String[] args) throws IOException{
		treeManager = new PatriciaTreeManager();
		transactionsGlobal = new ArrayList<TransactionList>();

		String startFolder = "/home/cloudera/Inputs/Denser";

		List<String> trees = new ArrayList<String>();
		File[] f = (new File(startFolder)).listFiles();

		for (int i = 0; i < f.length; i++) {
			if (f [i].isFile()) {
				trees.add(f[i].getAbsolutePath());
			}
		}

		//		List<Integer> kValues = new ArrayList<Integer>();
		//		kValues.add(10);
		//		kValues.add(100);
		//		kValues.add(1000);
		//		kValues.add(2000);
		//		kValues.add(3000);
		//		kValues.add(4000);
		//		kValues.add(5000);
		//		kValues.add(6000);
		//		kValues.add(7000);
		//		kValues.add(8000);
		//		kValues.add(9000);
		//		kValues.add(10000);
		//		kValues.add(20000);
		//		kValues.add(50000);
		//		kValues.add(100000);
		//		
		//		List<String> resultStrings = new ArrayList<String>();
		//
		//		for (String treePath : trees){
		//			PatriciaTree t = generatePatriciaTree(treePath, false);
		//			int nodeCount = t.getNumberOfNodes();
		//			for (int j = 0; j < 10; j++){
		//				for (int kVal : kValues){
		//					long patriciaInsertStart = new Date().getTime();
		//					treeManager.minTopKValues(t, kVal);
		//					long patriciaInsertDuration = new Date().getTime() - patriciaInsertStart;
		//					resultStrings.add(String.format("%d,%d,%d", nodeCount, kVal, patriciaInsertDuration));
		//					System.out.println(String.format("%d,%d,%d", nodeCount, kVal, patriciaInsertDuration));
		//				}
		//			}
		//		}
		//
		//		String join = String.join("\n", resultStrings);
		//		System.in.read();

		for (int i = 0; i < 10; i++){
			java.nio.file.Files.walk(Paths.get(startFolder)).forEach(path -> {
				String pathString = path.toString();
				if (!pathString.contains(".dat")){
					return;
				}

				String count = pathString.split("-")[1].split("\\.")[0];

				long patriciaBuildStart = new Date().getTime();
				generatePatriciaTree(pathString, false);
				long patriciaBuildEnd = new Date().getTime() - patriciaBuildStart;
				System.out.println(String.format("%s,%s", count, patriciaBuildEnd));	

				//			long patriciaInsertStart = new Date().getTime();
				//			currentTree = treeManager.mergeTree(currentTree, tree, false);
				//			long patriciaInsertDuration = new Date().getTime() - patriciaInsertStart;
				//
				//			System.out.println(String.format("transactions - %s, total branches - %s, branches - %s, nodes - %s, duration - %s", count, currentTree.getBranches(), tree.getBranches(), tree.getNodeCount(), patriciaInsertDuration));
				//			System.gc();
			});
		}
	}		

	private static PatriciaTree generatePatriciaTree(String inputPath, boolean debugMode){
		TransactionList transactions = IOHelper.loadTransactions(inputPath);
		transactionsGlobal.add(transactions);
		ItemSupportTable itemSupports = new ItemSupportTable(transactions);
		transactions = transactions.removeInfrequentItems(itemSupports, Constants.minSupport);
		transactions.orderTransactions(new TransactionListComparator(), itemSupports);
		return treeManager.buildTree(transactions, itemSupports, debugMode);
	}
}

