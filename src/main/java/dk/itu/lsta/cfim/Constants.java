package dk.itu.lsta.cfim;

public class Constants {
	public static final String pathSeparator = "/";
	public static final String baseFolder = System.getProperty("user.dir") + pathSeparator;
	public static final String inputFolder = baseFolder + String.format("src%smain%sresources%sData%s", pathSeparator, pathSeparator, pathSeparator, pathSeparator);
	public static final String patriciaTreeTestFolder = baseFolder + String.format("TestData%sPatriciaTrees%s", pathSeparator, pathSeparator);
	public static final  String filePath1 = inputFolder + "input.dat";
	public static final String filePath2 = inputFolder + "input2.dat";
	public static final  String filePath3 = inputFolder + "input3.dat";
	public static final String filePath4 = inputFolder + "input4.dat";
	public static final String filePath5 = inputFolder + "input5.dat";
	public static final String filePath6 = inputFolder + "input6.dat";
	public static final String filePath7 = inputFolder + "input7.dat";
	public static final String filePath8 = inputFolder + "input8.dat";
	public static final String filePath9 = inputFolder + "input9.dat";
	public static final String descriptions = inputFolder + "descriptions.dat";
	public static final int minSupport = 1;
	public static final int minLength = 0;
	public static final String separator = "-";
}
