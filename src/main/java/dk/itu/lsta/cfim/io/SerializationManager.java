package dk.itu.lsta.cfim.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.google.gson.Gson;

import dk.itu.lsta.cfim.entities.PatriciaTree;

public class SerializationManager {	

	private Gson gson;

	public SerializationManager(){
		this.gson = new Gson();
	}

	public String serializeAsJson(Object obj){
		return gson.toJson(obj);
	}

	public PatriciaTree deserializePatriciaTree(String text){
		return gson.fromJson(text, PatriciaTree.class);
	}

	public void serializeAsBinary(Object obj, String fileName) throws IOException {
		FileOutputStream fos = new FileOutputStream(fileName);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(obj);
		fos.close();
	}

	public PatriciaTree deserializeBinaryPatriciaTree(String fileName){
		ObjectInputStream ois = null;
		PatriciaTree patriciaTree = null;
		try {
			FileInputStream fis = new FileInputStream(fileName);
			ois = new ObjectInputStream(fis);
			patriciaTree = (PatriciaTree)ois.readObject();
			ois.close();
		} catch (Exception e){
			return null;
		}
		
		return patriciaTree;
	}
}
