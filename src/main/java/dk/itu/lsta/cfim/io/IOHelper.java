package dk.itu.lsta.cfim.io;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.TreeSet;

import com.google.gson.Gson;

import dk.itu.lsta.cfim.Constants;
import dk.itu.lsta.cfim.entities.Transaction;
import dk.itu.lsta.cfim.entities.TransactionList;

public class IOHelper {

	public static TransactionList loadTransactions(String filePath) {
		TransactionList transactions = new TransactionList();
		try {
			Scanner in = new Scanner(new FileReader(filePath));
			while(in.hasNextLine()){
				String line = in.nextLine();
				StringTokenizer tokenizer = new StringTokenizer(line);
				Transaction transaction = new Transaction();
				while (tokenizer.hasMoreElements()) {
					String nextToken = tokenizer.nextToken();
					if (Constants.minLength != 0 && nextToken.length() > Constants.minLength){
						transaction.add(nextToken);
						continue;
					}
					else{
						transaction.add(nextToken);	
					}
				}

				transactions.add(transaction);
			}

			in.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return transactions;
	}

	public static String readFile(String path, Charset encoding) 
	{
		byte[] encoded;
		try {
			encoded = Files.readAllBytes(Paths.get(path));
			return new String(encoded, encoding);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new String();
	}
}
