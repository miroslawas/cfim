package dk.itu.lsta.cfim.hbase;

import java.io.IOException;

import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.util.Bytes;

public class HBaseTest {
	 
    @SuppressWarnings("deprecation")
	public static void main(String[] args) throws IOException{
    	String identificatorsTableName = "identificators";
    	String rowIdentifier = "cfim";
    	String columnIdentifier = "id";
    	HBaseConnectionManager connManager = new HBaseConnectionManager();
    	connManager.clearTable(identificatorsTableName, rowIdentifier);
//    	
    	for (int i = 1; i < 11; i++){
    		connManager.insertValue(identificatorsTableName, rowIdentifier, columnIdentifier, String.valueOf(i));
    	}
//    	
//    	connManager.queryTable(identificatorsTableName, rowIdentifier);
    	Cell lastEntry = connManager.getLastEntry(identificatorsTableName, rowIdentifier);
    	if (lastEntry != null){
    		String trim = Bytes.toString(lastEntry.getValue()).trim();
        	Integer integer = Integer.parseInt(trim);
        	System.out.println(integer);
    	}
    }
}