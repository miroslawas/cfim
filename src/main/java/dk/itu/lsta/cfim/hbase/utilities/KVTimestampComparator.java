package dk.itu.lsta.cfim.hbase.utilities;

import java.util.Comparator;

import org.apache.hadoop.hbase.KeyValue;

public class KVTimestampComparator implements Comparator<KeyValue> {

	@Override
	public int compare(KeyValue kv1, KeyValue kv2) {
		Long kv1Timestamp = kv1.getTimestamp();
		Long kv2Timestamp = kv2.getTimestamp();

		return kv1Timestamp.compareTo(kv2Timestamp);
	}
}

