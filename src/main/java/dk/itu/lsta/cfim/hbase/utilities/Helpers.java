package dk.itu.lsta.cfim.hbase.utilities;

import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.util.Bytes;

public class Helpers {

	public static int convertCellToType(Cell cellValue) {
		String cellString = Bytes.toString(cellValue.getValue());
		return Integer.parseInt(cellString);
	}
}
