package dk.itu.lsta.cfim.hbase;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.TableExistsException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.TableNotFoundException;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hdfs.server.namenode.nn_005fbrowsedfscontent_jsp;

import dk.itu.lsta.cfim.hbase.utilities.KVTimestampComparator;

public class HBaseConnectionManager {
	private Configuration conf;
	private Admin admin;
	private Connection connection;

	public HBaseConnectionManager(){
		conf = HBaseConfiguration.create();
		try {
			connection = ConnectionFactory.createConnection(conf);
			admin = connection.getAdmin();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
	}

	public HTableDescriptor createTable(String tableName, String rowIdentifier){

		try {
			HTableDescriptor tableDescriptor = new HTableDescriptor(TableName.valueOf(tableName));
			tableDescriptor.addFamily(new HColumnDescriptor(rowIdentifier));
			admin.createTable(tableDescriptor);
			return admin.getTableDescriptor(TableName.valueOf(tableName));
		} 
		catch (TableExistsException e){
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}

		return null;
	}

	public HTableDescriptor getTable(String tableName){
		try {
			return admin.getTableDescriptor(TableName.valueOf(tableName));
		} catch (TableNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}

		return null;
	}

	public boolean clearTable(String tableName, String rowIdentifier){
		try {
			Table table = connection.getTable(TableName.valueOf(tableName));
			Delete delete = new Delete(Bytes.toBytes(rowIdentifier));
			table.delete(delete);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}

		return false;
	}

	public boolean tableExists(String tableName) {
		try {
			boolean tableExists = admin.tableExists(TableName.valueOf(tableName));
			return tableExists;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}

		return false;
	}

	public List<KeyValue> queryTable(String tableName, String rowIdentifier){
		Result result = null;
		try {
			Table table = connection.getTable(TableName.valueOf(tableName));
			Get g = new Get(Bytes.toBytes(rowIdentifier));
			result = table.get(g);
			List<KeyValue> row = Arrays.asList(result.raw());
			Collections.sort(row, new KVTimestampComparator());
			return row;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}

		return null;
	}

	public Cell getLastEntry(String tableName, String rowIdentifier){
		Result result = null;
		try {
			Table table = connection.getTable(TableName.valueOf(tableName));
			Get g = new Get(Bytes.toBytes(rowIdentifier));
			result = table.get(g);
			List<KeyValue> row = Arrays.asList(result.raw());
			if (row.isEmpty()){
				return null;
			}
			
			Collections.sort(row, new KVTimestampComparator());
			if (result != null && result.listCells() != null){
				int size = result.listCells().size();
				return row.get(size - 1);
			}

			return null;
		}
		catch (Exception e){
			e.printStackTrace();
		}

		return null;
	}

	public boolean insertValue(String tableName, String rowIdentifier, String column, String value) {
		boolean success = true;
		try {
			Table table = connection.getTable(TableName.valueOf(tableName));
			Put p = new Put(Bytes.toBytes(rowIdentifier)); 
			p.addColumn(Bytes.toBytes(column), Bytes.toBytes(value), Bytes.toBytes(value));
			table.put(p);
			return success;

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}

		return success;
	}

	public void deleteTable(String tableName) {
		try {
			admin.disableTable(TableName.valueOf(tableName));
			admin.deleteTable(TableName.valueOf(tableName));
		} catch (MasterNotRunningException e) {
			e.printStackTrace();
		} catch (ZooKeeperConnectionException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}  catch (Exception e){
			e.printStackTrace();
		}
	}
}
