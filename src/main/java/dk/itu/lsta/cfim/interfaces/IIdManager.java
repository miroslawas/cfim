package dk.itu.lsta.cfim.interfaces;

import java.util.List;

public interface IIdManager {

	public int incrementId();

	public int getLatestId();
	
	public List<Integer> getAllIds();
}