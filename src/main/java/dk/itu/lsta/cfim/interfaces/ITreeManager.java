package dk.itu.lsta.cfim.interfaces;

import java.util.HashMap;
import java.util.List;

import dk.itu.lsta.cfim.entities.PatriciaTree;
import dk.itu.lsta.cfim.entities.TransactionList;
import dk.itu.lsta.cfim.entities.TreeStructure;
import dk.itu.lsta.cfim.entities.Tuple;

public interface ITreeManager {
	
	/**
	 * @param transactions
	 * @param itemSupports
	 * @param debugMode 
	 * @return builds Patricia tree based on PatriciaInsert algorithm
	 */
	PatriciaTree buildTree(TransactionList transactions, HashMap<String, Integer> itemSupports, boolean isDebugMode);
	
	/**
	 * @param tree1
	 * @param tree2
	 * @param b 
	 * @return merges two Patricia trees based on PatriciaMerge algorithm
	 */
	PatriciaTree mergeTree(PatriciaTree tree1, PatriciaTree tree2, boolean isDebugMode);
	
	/**
	 * This method mines top-k closed itemsets from the Patricia tree
	 * @param tree - Patricia tree containing itemsets
	 * @param k 
	 * @return at least k frequent closed itemsets
	 */
	List<Tuple<String, Integer>> minTopKValues(PatriciaTree tree, int k);
	
}