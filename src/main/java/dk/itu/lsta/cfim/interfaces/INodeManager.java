package dk.itu.lsta.cfim.interfaces;

import java.util.Set;

import dk.itu.lsta.cfim.entities.Node;

public interface INodeManager {

	//Removes disjunction data from parentNode
	Node cleanParentNode(Node parentNode);

	//Removes parent data values from disjunctionNode
	Node cleanDisjunctionNode(Node disjunctionNode, String disjunctions);

	void promoteNode(Node node, Set<String> intersection, int supportIncrementValue);

	Node createNode();
	
	Node createNode(Set<String> parentIntersectionRemainder, int support);

	Node cloneNode(Node node);

	void transferChildrenNodes(Node sourceNode, Node targetNode);

	void insertDisjunctionNode(Set<String> parentIntersectionRemainder, Node node, int support);

	void changeNodeValue(Node candidateRoot, Set<String> candidateIntersectionRemainder);	
}