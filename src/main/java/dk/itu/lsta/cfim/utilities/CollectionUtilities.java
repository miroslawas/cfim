package dk.itu.lsta.cfim.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import dk.itu.lsta.cfim.entities.Transaction;
import dk.itu.lsta.cfim.entities.TransactionList;
import dk.itu.lsta.cfim.entities.Tuple;

public class CollectionUtilities {
	/**
	 * @param charArray
	 * @return converts char[] to Set<Character>
	 */
	public static Set<Character> convertToSet(char[] charArray) {
		Set<Character> resultSet = new HashSet<Character>();
		for (int i = 0; i < charArray.length; i++) {
			resultSet.add(new Character(charArray[i]));
		}
		return resultSet;
	}

	/**
	 * @param string
	 * @return converts string to Set<String> that represents characters of word
	 */
	public static Set<String> convertToSet(String string) {
		Set<String> resultSet = new HashSet<String>();
		for (int i = 0; i < string.length(); i++) {
			resultSet.add(string.substring(i, i + 1));
		}
		return resultSet;
	}

	/**
	 * @param transactions
	 * @param intersection
	 * @return returns empty closure of transactions
	 */
	public static Vector<String> getGlobalIntersection(ArrayList<Transaction> transactions, Vector<String> intersection){
		for (TreeSet<String> itemset : transactions) {
			intersection.retainAll(itemset);
			if (intersection.isEmpty()){
				return new Vector<String>();
			}
		}
		
		return intersection;
	}

	/**
	 * @param childrenValues
	 * @param candidateItem
	 * @return returns the 
	 */
	public static Tuple<Set<String>, Integer> getLargestIntersection(ArrayList<Set<String>> childrenValues, Set<String> candidateItem){
		Tuple<Set<String>, Integer> result = new Tuple<Set<String>, Integer>(new HashSet<String>(), -1);
		int index = 0;
		
		for (Set<String> item : childrenValues) {
			Set<String> commonSubstring = StringUtilities.getCommonCharacters(item, candidateItem);
			if (commonSubstring.size() > result.item1.size()){
				result.item1 = commonSubstring;
				result.item2 = index;
			}
			index++;
		}
		
		return result;
	}

	public static Set<String> getSetIntersection(Set<String> value, Set<String> value2) {
		if (value.equals(value2)){
			return value;
		}
		
		Set<String> tempValue1 = new HashSet<String>(value);
		Set<String> tempValue2 = new HashSet<String>(value2);
		
		if (tempValue1.retainAll(tempValue2)){
			return tempValue1;
		}
		
		if (tempValue2.retainAll(tempValue1)){
			return tempValue2;
		}
		
		return new HashSet<String>();
	}
}
