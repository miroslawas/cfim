package dk.itu.lsta.cfim.utilities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

public class StringUtilities {
	//Joining set of Strings
	public static String joinSet(Set<String> set, String sep) {
	    String result = null;
	    if(set != null) {
	        StringBuilder sb = new StringBuilder();
	        Iterator<String> it = set.iterator();
	        if(it.hasNext()) {
	            sb.append(it.next());
	        }
	        while(it.hasNext()) {
	            sb.append(sep).append(it.next());
	        }
	        
	        result = sb.toString();
	    }
	    
	    return result;
	}

	public static List<String> getWordsInString(String value){
		List<String> words = new ArrayList<String>();
		StringTokenizer tokens = new StringTokenizer(value);
		while(tokens.hasMoreTokens()){
			words.add(tokens.nextToken());
		}
		
		return words;
	}
	
	//Joining list of Strings
	public static String joinArray(ArrayList<String> list, String sep) {
	    String result = null;
	    if(list != null) {
	        StringBuilder sb = new StringBuilder();
	        Iterator<String> it = list.iterator();
	        if(it.hasNext()) {
	            sb.append(it.next());
	        }
	        while(it.hasNext()) {
	            sb.append(sep).append(it.next());
	        }
	        result = sb.toString();
	    }
	    return result;
	}
	
	public static Set<String> getCommonCharacters(Set<String> x, Set<String> y){
		if (x.equals(y)){
			return x;
		}
		
		Set<String> tempX = new HashSet<String>(x);
		Set<String> tempY = new HashSet<String>(y);
		
		if (tempX.retainAll(tempY))
		{
			return tempX;
		}

		if (tempY.retainAll(tempX))
		{
			return tempX;
		}

		return new HashSet<String>();
	}
	
	public static Set<Character> convertStringToCharSet(String inputString){
		Set<Character> result = new HashSet<Character>();
		for (int i = 0; i < inputString.length(); i++) {
			result.add(new Character(inputString.charAt(i)));
		}
		return result ;	
	}

	public static Vector<String> convertStringToStringVector(String inputString){
		Vector<String> result = new Vector<String>();
		for (int i = 0; i < inputString.length(); i++) {
			result.add(inputString.substring(i, i+1));
		}
		return result ;	
	}

	public static Set<String> getDisjunction(Set<String> parentValue, Set<String> commonCharacters) {
		Set<String> newParentValue = new HashSet<String>(parentValue);
		Set<String> newCommonCharacters = new HashSet<String>(commonCharacters);
		Iterator<String> itemsIterator = newParentValue.iterator();
		while (itemsIterator.hasNext()){
			String item = itemsIterator.next();
			if (newCommonCharacters.contains(item)){
				itemsIterator.remove();
			}
		}

		return newParentValue;
	}

	public static String joinSet(Vector<String> stringVector, String sep) {
		String resultString = "";
		for (int i = 0; i < stringVector.size(); i++) {
			resultString += stringVector.elementAt(i) + sep;
		}
		return resultString;
	}
}
