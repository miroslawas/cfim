package dk.itu.lsta.cfim.utilities;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

public class TransactionListComparator implements Comparator<TreeSet<String>> {

	private HashMap<String, Integer> itemSupports;
	private int totalItemSupport;
	
	public void setSupports(HashMap<String, Integer> transactionSupports){
		this.itemSupports = transactionSupports;
		
		for (int value : itemSupports.values()) {
			totalItemSupport += value;
		}
	}
	
	public int compare(TreeSet<String> t1, TreeSet<String> t2) {
		float t1RelativeSupport = (float)getTransactionSupports(t1) / (float)totalItemSupport;		
		float t2RelativeSupport = (float)getTransactionSupports(t2) / (float)totalItemSupport;
		
		if (t1RelativeSupport > t2RelativeSupport){
			return -1;
		}
		
		if (t1RelativeSupport < t2RelativeSupport){
			return 1;
		}
		
		return 0;
	}
	
	private int getTransactionSupports(TreeSet<String> transaction){
		Integer totalSupport = 0;
		Iterator<String> iterator = transaction.iterator();
		while(iterator.hasNext()){
			String t1Item = iterator.next();
			totalSupport += itemSupports.get(t1Item);
		}
		return totalSupport;
	}
}
