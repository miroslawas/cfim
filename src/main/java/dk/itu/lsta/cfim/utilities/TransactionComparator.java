package dk.itu.lsta.cfim.utilities;

import java.util.Comparator;

import dk.itu.lsta.cfim.entities.ItemSupportTable;

public class TransactionComparator implements Comparator<String> {

	private ItemSupportTable itemSupports;

	public int compare(String item1, String item2) {
		if (!itemSupports.containsKey(item1) || !itemSupports.containsKey(item2)){
			return 0;
		}
		
		if (itemSupports.get(item1) > itemSupports.get(item2)){
			return -1;
		}
		
		if (itemSupports.get(item1) < itemSupports.get(item2)){
			return 1;
		}
		
		if (!item1.equals(item2)){
			return -1;
		}
		
		return 0;
	}

	public void setItemSupports(ItemSupportTable itemSupports) {
		this.itemSupports = itemSupports;
	}

}
