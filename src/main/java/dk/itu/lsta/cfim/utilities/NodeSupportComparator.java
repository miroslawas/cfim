package dk.itu.lsta.cfim.utilities;


import java.util.Comparator;

import dk.itu.lsta.cfim.entities.Node;

public class NodeSupportComparator implements Comparator<Node> {

	@Override
	public int compare(Node node1, Node node2) {
		if (node1.getSupport() > node2.getSupport()){
			return -1;
		}
		else if (node2.getSupport() > node1.getSupport()){
			return 1;
		}
		
		return 0;
	}
}
