package dk.itu.lsta.cfim.logic;

import java.util.HashSet;
import java.util.Set;

import dk.itu.lsta.cfim.entities.Node;
import dk.itu.lsta.cfim.interfaces.IIdManager;
import dk.itu.lsta.cfim.interfaces.INodeManager;

public class NodeManager implements INodeManager {
	private IIdManager idManager;

	public NodeManager() {
		idManager = new PatriciaIdManager();
	}

	public Node createNode(){
		int nodeId = idManager.incrementId();
		return new Node(nodeId);
	}

	public Node createNode(Set<String> nodeValue, int support) {
		int nodeId = idManager.incrementId();
		Node node = new Node(nodeValue, support);
		node.setId(nodeId);
		return node;
	}

	public Node cloneNode(Node node){
		Node clonedNode = createNode();

		if (node.hasChildren()){
			for (Node child : node.getChildrenNodes()){
				clonedNode.insertChild(cloneNode(child));
			}
		}

		clonedNode.setSupport(node.getSupport());
		clonedNode.setValue(node.getValue());

		return clonedNode;
	}

	/* (non-Javadoc)
	 * @see dk.itu.lsta.cfim.logic.INodeManager#RemoveParentDisjunctions(dk.itu.lsta.cfim.Entities.Node, java.lang.String)
	 */
	public Node cleanParentNode(Node parentNode){
		parentNode.setSupport(0);
		parentNode.setValue(new HashSet<String>());
		return parentNode;
	}

	/* (non-Javadoc)
	 * @see dk.itu.lsta.cfim.logic.INodeManager#CleanDisjunction(dk.itu.lsta.cfim.Entities.Node, java.lang.String)
	 */
	public Node cleanDisjunctionNode(Node disjunctionNode, String disjunctions){	
		disjunctionNode.trimValue(disjunctions);
		return disjunctionNode;
	}

	public void transferChildrenNodes(Node sourceNode, Node targetNode) {
		if (sourceNode.hasChildren()){
			for (Node child : sourceNode.getChildrenNodes()){
				targetNode.insertChild(cloneNode(child));
			}
		}

		sourceNode.removeChildrenNodes();
		sourceNode.insertChild(targetNode);
	}

	/* (non-Javadoc)
	 * @see dk.itu.lsta.cfim.Interfaces.INodeManager#promoteNode(dk.itu.lsta.cfim.Entities.Node, java.lang.String)
	 */
	public void promoteNode(Node node, Set<String> intersection, int supportIncrementValue) {
		// TODO Auto-generated method stub
		node.incrementSupport(supportIncrementValue);
		changeNodeValue(node, intersection);
	}

	public void changeNodeValue(Node node, Set<String> newValue){
		node.setValue(newValue);
	}

	public void insertDisjunctionNode(Set<String> parentIntersectionRemainder, Node node, int support) {
		Node disjunctionNode = createNode(parentIntersectionRemainder, support);
		if (node.hasChildren()){
			transferChildrenNodes(node, disjunctionNode);
		}
		else{
			node.insertChild(disjunctionNode);
		}
	}
}
