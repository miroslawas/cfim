package dk.itu.lsta.cfim.logic;

import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.DebugGraphics;

import org.apache.hadoop.hbase.util.Hash;

import dk.itu.lsta.cfim.Constants;
import dk.itu.lsta.cfim.entities.Node;
import dk.itu.lsta.cfim.entities.PatriciaTree;
import dk.itu.lsta.cfim.entities.Transaction;
import dk.itu.lsta.cfim.entities.TransactionList;
import dk.itu.lsta.cfim.entities.Tuple;
import dk.itu.lsta.cfim.interfaces.INodeManager;
import dk.itu.lsta.cfim.interfaces.ITreeManager;
import dk.itu.lsta.cfim.utilities.CollectionUtilities;
import dk.itu.lsta.cfim.utilities.NodeSupportComparator;
import dk.itu.lsta.cfim.utilities.StringUtilities;

/**
 * @author Laurynas-PC
 *
 */
public class PatriciaTreeManager implements ITreeManager {

	private INodeManager nodeManager;
	
	public PatriciaTreeManager(){
		nodeManager = new NodeManager();
	}

	/* (non-Javadoc)
	 * @see dk.itu.lsta.cfim.logic.ITreeManager#buildTree(dk.itu.lsta.cfim.Entities.TransactionList, java.util.HashMap)
	 */
	public PatriciaTree buildTree(TransactionList transactions, HashMap<String,Integer> itemSupports, boolean isDebugMode) {

		//Creating an empty Patricia tree
		PatriciaTree tree = new PatriciaTree(); 

		//Finding empty closure
		Vector<String> emptyClosure = CollectionUtilities.getGlobalIntersection(transactions, new Vector<String>(itemSupports.keySet()));
		tree.setRootNode(nodeManager.createNode());
		Node rootNode = tree.getRootNode();

		//If closure exist
		if (emptyClosure.size() > 0){

			Set<String> eClos = new HashSet<String>(emptyClosure);
			if (isDebugMode){
				System.out.println(String.format("%d. Empty closure %s \n", 0, StringUtilities.joinSet(eClos, "")));
			}
			
			//Setting root to closure
			rootNode.setValue(eClos);
			rootNode.setSupport(transactions.size());

			//Remove closure from transactions
			transactions = removeEmptyClosure(transactions, eClos);
		}else {
			//Empty root
			rootNode.setValue(new HashSet<String>());
		}

		//Main check if there are any transactions available
		Iterator<Transaction> transactionsIterator = transactions.iterator();
		int i = 1;
		int transactionsCount = transactions.size();
		while (transactionsIterator.hasNext()){
			Transaction transaction = transactionsIterator.next();
			if (isDebugMode){
				System.out.println(String.format("%d. Inserting new transaction %s \n", i, transaction.toString()));
			}

			insertTransaction(rootNode, transaction, isDebugMode);

			if (isDebugMode){
				tree.printPatriciaTree();
			}
			i++;
		} 

		tree.setRootNode(rootNode);
		tree.updateTreeInfo();
		
		if (isDebugMode){
			System.out.println(String.format("Created Patricia tree containing %d transactions", i));
		}
		
		return tree;
	}

	public void insertTransaction(Node parentNode, Transaction transaction, boolean isDebugMode) {
		if (transaction.isEmpty()){
			return;
		}

		//Getting candidate node by finding largest intersection of values
		Tuple<Set<String>,Integer> candidateKey =  findCandidate(transaction.getValue(), parentNode);
		Set<String> intersection = candidateKey.item1;

		//Candidate was not found and transaction is inserted as a child to the parent
		if (intersection.isEmpty()){
			parentNode.insertChild(nodeManager.createNode(transaction, transaction.getSupport()));
			return;
		}

		//Checking if candidate is complete subset of parent
		parentNode = parentNode.getItemByIndex(candidateKey.item2);

		//Is candidate value equals to transaction  (i.e. transaction = {A}, parent = {A})
		if (areEqual(intersection, parentNode.getValue())){
			parentNode.incrementSupport();
		}

		//Finding different items in parentNode and transaction 
		Set<String> transactionRemainder = StringUtilities.getDisjunction(transaction, intersection);
		Set<String> parentRemainder = StringUtilities.getDisjunction(parentNode.getValue(), intersection);
	
		if (transactionRemainder.isEmpty() && parentRemainder.isEmpty()){
			return;
		}
		
		//Creating a disjunction node (i.e. {ABC|4} becomes {AB|5}->{C|4}
		if (!parentRemainder.isEmpty()){
			nodeManager.insertDisjunctionNode(parentRemainder, parentNode, parentNode.getSupport());
			nodeManager.promoteNode(parentNode, intersection, 1);
		}
		else if (!parentNode.hasChildren()){
			parentNode.incrementSupport();
			parentNode.insertChild(nodeManager.createNode(parentRemainder, parentNode.getSupport() - 1));	
			transaction.removeAll(intersection);
			insertTransaction(parentNode, transaction, isDebugMode);
			return;
		}

		if (transactionRemainder.size() > 0){
			transaction.removeAll(intersection); 
			insertTransaction(parentNode, transaction, isDebugMode);
		}
	}

	/* (non-Javadoc)
	 * @see dk.itu.lsta.cfim.logic.ITreeManager#mergeTree(dk.itu.lsta.cfim.Entities.TreeStructure, dk.itu.lsta.cfim.Entities.TreeStructure)
	 */
	public PatriciaTree mergeTree(PatriciaTree t1, PatriciaTree t2, boolean isDebugMode) {

		Node parentRoot = t1.getRootNode();
		Node candidateRoot = t2.getRootNode();
		Set<String> intersection = getNodeIntersection(parentRoot, candidateRoot);
		
		if (candidateRoot.getStringValue().isEmpty()){
			for (Node child : candidateRoot.getChildrenNodes()){
				branchMerge(parentRoot, child, isDebugMode);
			}
			
			//TODO: decomment this line
			//t1.updateTreeInfo();
			return t1;
		}
		
		//Trees does not share the same root (no common prefix exist)
		if (intersection.isEmpty()){
			if (!parentRoot.getStringValue().isEmpty()){
				nodeManager.insertDisjunctionNode(parentRoot.getValue(), parentRoot, parentRoot.getSupport());
				nodeManager.cleanParentNode(parentRoot);
				parentRoot = t1.getRootNode();
			}
			
			parentRoot.insertChild(candidateRoot);
			return t1;
		}
		
		//Trees have the same value at root node
		if (areEqual(intersection, parentRoot.getValue()) &&  areEqual(intersection, candidateRoot.getValue())){
			parentRoot.incrementSupport(candidateRoot.getSupport());
			for (Node child : candidateRoot.getChildrenNodes()){
				branchMerge(parentRoot, child, isDebugMode);
			}
			
			//TODO: decomment this line
			//t1.updateTreeInfo();
			return t1;
		}
		
		Set<String> parentIntersectionRemainder = StringUtilities.getDisjunction(parentRoot.getValue(), intersection);
		Set<String> candidateIntersectionRemainder = StringUtilities.getDisjunction(candidateRoot.getValue(), intersection);
		int originalParentSupport = parentRoot.getSupport();
		nodeManager.promoteNode(parentRoot, intersection, candidateRoot.getSupport());
		
		
		if (!parentIntersectionRemainder.isEmpty()){			
			nodeManager.insertDisjunctionNode(parentIntersectionRemainder, parentRoot, originalParentSupport);
		}
		
		if (!candidateIntersectionRemainder.isEmpty()){
			nodeManager.changeNodeValue(candidateRoot, candidateIntersectionRemainder);
			branchMerge(parentRoot, candidateRoot, isDebugMode);
		}
		//Tree2 root is identical to the Tree1 root and therefore its children will be inserted into Tree1 
		else{
			for (Node child : candidateRoot.getChildrenNodes()){
				branchMerge(parentRoot, child, isDebugMode);
			}
		}
		
		//TODO: decomment this line
		//t1.updateTreeInfo();
		return t1;
	}

	//Merges two Patricia tree branches
	private void branchMerge(Node parentNode, Node candidateNode, boolean isDebugMode) {
		if (isDebugMode){
			System.out.println(String.format("Merging nodes {%s} and {%s} \n", parentNode.getStringValue(), candidateNode.getStringValue()));
		}
		
		Tuple<Set<String>, Integer> candidate = findCandidate(candidateNode.getValue(), parentNode);
		if (candidate.item1.isEmpty()){
			parentNode.insertChild(candidateNode);
			return;
		}
		else{
			parentNode = parentNode.getItemByIndex(candidate.item2);
			Set<String> intersection = getNodeIntersection(parentNode, candidateNode);
			if (intersection.isEmpty()){
				parentNode.incrementSupport(candidateNode.getSupport());
				for(Node child : candidateNode.getChildrenNodes()){
					branchMerge(parentNode, child, isDebugMode);
				}
				return;
			}	
			else{
				Set<String> parentIntersectionRemainder = StringUtilities.getDisjunction(parentNode.getValue(), intersection);
				Set<String> candidateIntersectionRemainder = StringUtilities.getDisjunction(candidateNode.getValue(), intersection);
				
				int originalParentSupport = parentNode.getSupport();
				nodeManager.promoteNode(parentNode, intersection, candidateNode.getSupport());

				if (parentIntersectionRemainder.isEmpty() && candidateIntersectionRemainder.isEmpty()){
					return;
				}
				
				nodeManager.insertDisjunctionNode(parentIntersectionRemainder, parentNode, originalParentSupport);
				if (!parentIntersectionRemainder.isEmpty()){			
				}
				
				if (!candidateIntersectionRemainder.isEmpty()){
					nodeManager.changeNodeValue(candidateNode, candidateIntersectionRemainder);
					branchMerge(parentNode, candidateNode, isDebugMode);
				}else{
					for (Node child : candidateNode.getChildrenNodes()){
						branchMerge(parentNode, child, isDebugMode);
					}
				}
			}
		}
		
		return;
	}

	public List<Tuple<String, Integer>> minTopKValues(PatriciaTree tree, int k){
		PriorityQueue<Node> frontier = new PriorityQueue<Node>(new NodeSupportComparator());
		HashMap<Integer, String> idMappings = new HashMap<>();
		List<Tuple<String, Integer>> returnValues = new ArrayList<Tuple<String, Integer>>();
		
		if (!tree.getRootNode().getStringValue().isEmpty()) {
			frontier.add(tree.getRootNode());
		} else{
			frontier.addAll(tree.getRootNode().getChildrenNodes());
		}
		
		if (tree.getNumberOfNodes() < k){
			k = tree.getNumberOfNodes();
		}
		
		int minedItems = 0;
		while(!frontier.isEmpty() && minedItems < k){
			Node currentNode = frontier.poll();

			if (minedItems + frontier.size() <= k && currentNode.hasChildren()){
				ArrayList<Node> childrenNodes = currentNode.getChildrenNodes();
				frontier.addAll(childrenNodes);
			}
			
			int nextSupport = frontier.peek().getSupport();	
			
			//Checking whether current node has children nodes that have support value that is at least higher than the current node support
			List<Integer> childrenWithSupports = currentNode.getChildenWithSupport(nextSupport);
			if (!childrenWithSupports.isEmpty()){
				for(int childIndex : childrenWithSupports){
					if (frontier.contains(currentNode.getItemByIndex(childIndex))){
						frontier.add(currentNode.getItemByIndex(childIndex));
					}
				}
			}

			if(currentNode.getStringValue().isEmpty()){
				continue;
			}
						
			String nodeValue = currentNode.getBranchValue();
			
			if (idMappings.containsKey(currentNode.getParentId())){
				nodeValue = idMappings.get(currentNode.getParentId()) + Constants.separator + nodeValue;
			}
			
			idMappings.put(currentNode.getId(), nodeValue);
			
			returnValues.add(new Tuple<String, Integer>(nodeValue, currentNode.getSupport()));
			minedItems++;
		}
		
		return returnValues;
	}

	/**
	 * Finds a best node candidate for inserting a transaction 
	 * @param transaction
	 * @param set2
	 * @return a candidate parent node for a transaction
	 */
	private Tuple<Set<String>, Integer> findCandidate(Set<String> transaction, Node parentNode) {
		ArrayList<Set<String>> childrenValues = new ArrayList<Set<String>>();
		for (Node child : parentNode.getChildrenNodes()) {
			childrenValues.add(child.getValue());
		}
		return CollectionUtilities.getLargestIntersection(childrenValues, transaction);
	}
	
	private Set<String> getNodeIntersection(Node node1, Node node2){
		return CollectionUtilities.getSetIntersection(node1.getValue(), node2.getValue());
	}

	private TransactionList removeEmptyClosure(TransactionList transactions, Set<String> emptyClosure) {

		Iterator<Transaction> itr = transactions.iterator();
		while(itr.hasNext()){
			TreeSet<String> transaction = itr.next();
			transaction.removeAll(emptyClosure);
		}

		return transactions;
	}

	private boolean areEqual(Set<String> intersection, Set<String> stringValue) {
		return intersection.equals(stringValue);
	}
}
