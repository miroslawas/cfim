package dk.itu.lsta.cfim.logic;

import java.util.ArrayList;
import java.util.List;

import dk.itu.lsta.cfim.interfaces.IIdManager;

/**
 * @author Laurynas-PC
 *	This class assigns IDs and is responsible for ID logic for Patricia tree structures  
 */
public class PatriciaIdManager implements IIdManager {

	private int currentId;
	
	public PatriciaIdManager(){
		this.currentId = 1;
	}

	public int incrementId() {
		return currentId++;		
	}

	public int getLatestId() {
		return currentId;
	}

	@Override
	public List<Integer> getAllIds() {
		List<Integer> ids = new ArrayList<Integer>();
		for (int i = 1; i <= currentId; i++){
			ids.add(i);
		}
		
		return ids;
	}
}
