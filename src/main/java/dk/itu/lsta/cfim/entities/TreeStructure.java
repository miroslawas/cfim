package dk.itu.lsta.cfim.entities;

public class TreeStructure {
	private Node rootNode;
	private int numberOfNodes;
	private int branchCount;
	private int totalSupport;
	private String hashValue;
	
	public Node getRootNode() {
		return rootNode;
	}

	public void setRootNode(Node rootNode) {
		this.rootNode = rootNode;
	}

	public int getTotalSupport() {
		return totalSupport;
	}

	public void incrementTotalSupport(int incrementDelta){
		this.totalSupport += incrementDelta;
	}
	
	public void setTotalSupport(int totalSupport) {
		this.totalSupport = totalSupport;
	}
	
	public void clearTotalSupport(){
		this.totalSupport = 0;
	}

	public int getNumberOfNodes() {
		return numberOfNodes;
	}

	public void incrementNumberOfNodes(int incrementDelta){
		this.numberOfNodes += incrementDelta;
	}
	
	public void setNumberOfNodes(int numberOfNodes) {
		this.numberOfNodes = numberOfNodes;
	}
	
	public void clearNumberOfNodes(){
		this.numberOfNodes = 0;
	}

	public String getHashValue() {
		return hashValue;
	}

	public void setHashValue(String hashValue) {
		this.hashValue = hashValue;
	}

	public int getBranchCount() {
		return branchCount;
	}

	public void setBranchCount(int branchCount) {
		this.branchCount = branchCount;
	}
}
