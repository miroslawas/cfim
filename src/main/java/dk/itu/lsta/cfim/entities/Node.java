package dk.itu.lsta.cfim.entities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dk.itu.lsta.cfim.Constants;
import dk.itu.lsta.cfim.utilities.StringUtilities;
import dk.itu.lsta.cfim.utilities.NodeSupportComparator;

public class Node{

	private ArrayList<Node> children;
	private int support;
	private int id;
	private int parentId;
	private Set<String> value;
	//	private Node parentNode;
	private boolean isRootNode;
	private NodeSupportComparator nodeSupportComparator;

	//Base constructor
	public Node(){
		this.isRootNode = false;
		this.children = new ArrayList<Node>();
		this.nodeSupportComparator = new NodeSupportComparator();
		this.value = new HashSet<String>();
	}

	public Node(int id){
		this();
		setId(id);
	}

	//Constructor
	public Node(Set<String> keyValue, int supportValue){
		this();
		this.value = keyValue;
		this.support = supportValue;
	}

	public boolean hasChildren() {
		if (this.children == null || this.children.size() == 0){
			return false;
		}
		return true;
	}

	public ArrayList<Node> getChildrenNodes() {
		if (children != null){
			return children;
		}
		return null;
	}

	public Node getItemByIndex(int index){
		return children.get(index);
	}

	public void removeChild(String itemKey){
		Iterator<Node> iterator = children.iterator();
		while (iterator.hasNext()){
			Node currentNode = iterator.next();
			if (String.join("", currentNode.getValue()) == itemKey){
				iterator.remove();
				return;
			}
		}
	}

	public boolean hasChild(String itemKey){
		for (Node node : children) {
			if (String.join("", node.getValue()) == itemKey){
				return true;
			}
		}
		return false;
	}

	public Node getChild(String itemKey){
		for (Node node : children) {
			if (String.join("", node.getValue()) == itemKey){
				return node;
			}
		}
		return null;
	}

	public void setChildren(ArrayList<Node> items) {
		for (Node child : items){
			this.children.add(child);
		}
		this.children = items;
	}

	public void insertChild(Node node) {
		if (!node.getStringValue().isEmpty()) {
			node.setParentId(this.getId());
			this.children.add(node);
		} else {
			for (Node child : node.getChildrenNodes()){
				child.setParentId(this.getId());
				this.children.add(child);
			}
		}
		
		this.children.sort(nodeSupportComparator);
	}

	public int getSupport() {
		return support;
	}

	public void setSupport(Integer support) {
		this.support = support;
	}

	public Boolean getIsRootNode() {
		return isRootNode;
	}

	public void setAsRootNode(){
		this.isRootNode = true;
	}

	public String getStringValue()
	{
		return StringUtilities.joinSet(this.value, "");
	}

	public Set<String> getValue() {
		return value;
	}

	public void setValue(Set<String> value) {
		this.value = value;
	}

	public void trimValue(String value){
		for (String item : value.split("")) {
			if (this.value.contains(item)){
				this.value.remove(item);
			}
		}
	}

	public void removeChildrenNodes() {
		this.children.clear();
	}

	public void incrementSupport() {
		this.support++;		
	}

	public void incrementSupport(int supportDelta) {
		this.support += supportDelta;		
	}

	public int getId(){
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public String getStringValueWithSeparator(String separator) {
		if (separator.isEmpty()){
			return StringUtilities.joinSet(this.value, Constants.separator);	
		}

		return StringUtilities.joinSet(this.value, separator);
	}

	//	public Node getParentNode() {
	//		return parentNode;
	//	}
	//
	//	public void setParentNode(Node parentNode) {
	//		this.parentNode = parentNode;
	//	}

	public String getBranchValue(){
		return this.getStringValueWithSeparator(Constants.separator);

		//		boolean notFirstInsertion = true;
		//		Node currentNode = this;
		//		String branchValue = "";
		//		while (currentNode != null){
		//			if (notFirstInsertion)			{
		//				branchValue = currentNode.getStringValueWithSeparator(Constants.separator) + branchValue;
		//				notFirstInsertion = false;
		//			}
		//			else{
		//				branchValue = currentNode.getStringValueWithSeparator(Constants.separator) + Constants.separator + branchValue;
		//			}
		//			
		//			currentNode = currentNode.getParentNode();
		//		}
		
		//		
		//		branchValue.substring(0, branchValue.lastIndexOf(Constants.separator));
		//		return branchValue;
	}

	public List<Integer> getChildenWithSupport(int support) {
		List<Integer> nodes = new ArrayList<Integer>();
		for (Node childNode : this.getChildrenNodes())
		{
			if (childNode.getSupport() <= support){
				nodes.add(this.children.indexOf(childNode));
			}
		}

		return nodes;
	}
}