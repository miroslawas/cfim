package dk.itu.lsta.cfim.entities;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import dk.itu.lsta.cfim.utilities.StringUtilities;

public class Transaction extends TreeSet<String>{
	
	public Transaction(String value){
		this.addAll(StringUtilities.getWordsInString(value));
	}
	
	private String hash;
	private int support;
	
	public Transaction(){
		this.support = 1;
	}

	public Transaction(List<String> items) {
		super();
		this.addAll(items);
	}

	public Transaction(List<String> items, int support) {
		super();
		this.addAll(items);
		this.setSupport(support);
	}
	
	public Transaction(Comparator<String> comparator) {
		super(comparator);
	}

	public String getHash(){
		return this.hash;
	}
	
	public void setHash(){
		this.hash =  StringUtilities.joinSet(this, "-");
	}
	
	@Override
	public boolean add(String e) {
		boolean success = super.add(e);
		if (success)
		{
			setHash();
		}
		
		return success;
	}
	
	@Override
	public boolean addAll(Collection<? extends String> c) {
		boolean success = super.addAll(c);
		if (success){
			setHash();
		}
		
		return success;
	}
 
	@Override
	public boolean remove(Object o) {
		boolean success = super.remove(o);
		if (success){
			setHash();
		}

		return success;
	}
		
	/* 
	 * Removes all items from transactionList that exist in collection c
	 */
	@Override
	public boolean removeAll(Collection<?> c) {
		try{
			Iterator<String> iterator = this.iterator();
			while (iterator.hasNext()){
				String currentItem = iterator.next();
				if (c.contains(currentItem)){
					iterator.remove();
				}
			}

			setHash();
			return true;	
		}
		catch(Exception e){
			return false;
		}
	}

	public Integer getSupport() {
		return this.support;
	}
	
	private void setSupport(int support) {
		this.setSupport(support);
	}

	public Set<String> getValue() {
		return this;
	}	
}