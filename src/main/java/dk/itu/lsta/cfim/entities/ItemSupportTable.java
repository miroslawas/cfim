package dk.itu.lsta.cfim.entities;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ItemSupportTable extends HashMap<String, Integer> {
	public ItemSupportTable(){
	}
	
	public ItemSupportTable(List<Transaction> transactions){
		generateTable(new TransactionList(transactions));
	}
	
	public ItemSupportTable(TransactionList transactions) {
		generateTable(transactions);
	}

	public ItemSupportTable  generateTable(TransactionList transactions) {
		Iterator<Transaction> transactionIterator =  transactions.iterator();
		while(transactionIterator.hasNext()){
			Iterator<String> itemIterator = (transactionIterator.next()).iterator();
			while(itemIterator.hasNext()){
				String nextToken = itemIterator.next();
				if (!this.containsKey(nextToken)){
					this.put(nextToken, 1);
				}
				else{
					this.put(nextToken, this.get(nextToken) + 1);
				}
			}
		}
		
		return this;
	}
}
