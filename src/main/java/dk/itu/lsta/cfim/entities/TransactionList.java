package dk.itu.lsta.cfim.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import dk.itu.lsta.cfim.utilities.TransactionComparator;
import dk.itu.lsta.cfim.utilities.TransactionListComparator;

/**
 * @author Laurynas-PC
 *
 */
public class TransactionList extends ArrayList<Transaction>{
	
	private ItemSupportTable itemSupports;
	
	public TransactionList(){
	}

	public TransactionList(List<Transaction> transactions){
		super();
		this.addAll(transactions);
	}
	
	public TransactionList(String transactionsRaw){
		String[] tr = transactionsRaw.split("\n");
		for(String t : tr){
			this.add(new Transaction(t));
		}
	}
	
	/**
	 * @param itemSupports
	 * @param minSupport
	 * @return returns itemset with items whose support is lower than minSupport threshold removed
	 */
	public TransactionList removeInfrequentItems(HashMap<String, Integer> itemSupports, Integer minSupport) {
		Iterator<Transaction> transIterator = this.iterator();
		
		while (transIterator.hasNext()){
			Iterator<String> itemIterator = transIterator.next().iterator();
			
			while(itemIterator.hasNext()){
				String item = itemIterator.next();
				
				if (itemSupports.get(item).intValue() < minSupport){
					itemIterator.remove();
				}
			}
		}
		
		return this;
	}

	/**
	 * @param comparator
	 * @param itemSupports
	 * orders transactions by their length and total supports
	 */
	public void orderTransactions(TransactionListComparator comparator, ItemSupportTable itemSupports) {
		this.itemSupports = itemSupports;
		comparator.setSupports(itemSupports);
		this.sort(comparator);
		//this.sortItemsets();
	}
	
	private void sortItemsets(){
		 ArrayList<Transaction> sortedTransactions = new ArrayList<Transaction>();
		 TransactionComparator transactionComparator = new TransactionComparator();
		 transactionComparator.setItemSupports(itemSupports);
		 
		 Iterator<Transaction> transactionIterator = this.iterator();
		 while(transactionIterator.hasNext()){
			 Iterator<String> itemsIterator = transactionIterator.next().iterator();
			 Transaction newSet = new Transaction(transactionComparator);

			 while(itemsIterator.hasNext()){
				 String nextItem = itemsIterator.next();
				 newSet.add(nextItem);
			 }
			 
			 sortedTransactions.add(sortedTransactions.size(), newSet);
		 }
		 
		 this.removeRange(0, this.size());
		 this.addAll(sortedTransactions);		 
	}
}
