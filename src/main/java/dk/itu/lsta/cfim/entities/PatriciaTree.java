package dk.itu.lsta.cfim.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
	
public class PatriciaTree extends TreeStructure implements Serializable{
	
	private int id;
	
	public PatriciaTree(){
		this.createRootNode();
	}
	
	/**
	 * Creates an empty root node
	 */
	private void createRootNode() {
		Node rootNode = new Node();
		rootNode.setAsRootNode();
		this.setRootNode(rootNode);
	}
	
	public void updateTreeInfo(){
		StringBuilder hashBuilder = new StringBuilder();
		Queue<Node> frontier = new LinkedList<Node>();
		frontier.add(this.getRootNode());
		
		this.clearNumberOfNodes();
		this.clearTotalSupport();
		
		while (!frontier.isEmpty()){
			Node currentNode = frontier.poll();
			if (currentNode.hasChildren()){
				frontier.addAll(currentNode.getChildrenNodes());
			}
			
			this.incrementNumberOfNodes(currentNode.getSupport());
			this.incrementTotalSupport(currentNode.getSupport());
			
			//NodeId-ParentId-Support-Value
			hashBuilder.append(String.format("%d-%d-%d {%s} \n", currentNode.getId(), currentNode.getParentId(), currentNode.getSupport(), currentNode.getStringValueWithSeparator("")));
		}
		
		int branchCount = getBranches();
		this.setBranchCount(branchCount);
		this.setHashValue(hashBuilder.toString());
	}
	
	public int getBranches() {
		Queue<Node> nodes = new LinkedList<Node>();
		nodes.add(this.getRootNode());
		int branchCount = 0;
		
		while(!nodes.isEmpty()){
			Node node = nodes.poll();
			nodes.addAll(node.getChildrenNodes());
			if (!node.hasChildren()){
				branchCount++;
			}
		}
		
		return branchCount;
	}

	public void printPatriciaTree(){
		Queue<Node> frontier = new LinkedList<Node>();
		frontier.add(this.getRootNode());
		
		while (!frontier.isEmpty()){
			Node currentNode = frontier.poll();
			if (currentNode.hasChildren()){
				frontier.addAll(currentNode.getChildrenNodes());
			}
			
			String printValue = String.format(
					"Value: %s\n"
					+ "Id: %s\n"
					+ "Parent: %s\n"
					+ "Support: %s\n"
					+ "%s", 
					currentNode.getStringValueWithSeparator(""), currentNode.getId(), currentNode.getParentId(), currentNode.getSupport(), System.lineSeparator());

			System.out.print(printValue);
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNodeCount() {
		Queue<Node> frontier = new LinkedList<Node>();
		int nodeCount = 0;
		frontier.add(this.getRootNode());
		nodeCount++;
		
		while (!frontier.isEmpty()){
			Node currentNode = frontier.poll();
			if (currentNode.hasChildren()){
				frontier.addAll(currentNode.getChildrenNodes());
			}
			nodeCount++;
		}
		
		return nodeCount;
	}
}